/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDial>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_2;
    QLCDNumber *lcdNumber;
    QGridLayout *gridLayout_4;
    QPushButton *pushButton;
    QDial *ZNUVoltage;
    QGridLayout *gridLayout_2;
    QLabel *labelLeftSaved;
    QLabel *labelActual3;
    QSpacerItem *verticalSpacer;
    QLabel *label_12;
    QPushButton *pushButton_SaveCentre;
    QLabel *label_11;
    QPushButton *pushButton_SaveLeft;
    QLabel *labelActual;
    QLabel *labelRightSaved;
    QLabel *labelCenterSaved;
    QLabel *labelActual2;
    QLabel *label_8;
    QLabel *label_7;
    QLabel *label_10;
    QPushButton *pushButton_SaveRight;
    QLabel *label_9;
    QVBoxLayout *verticalLayout_4;
    QSpacerItem *verticalSpacer_3;
    QLabel *labelDegrees;
    QSpacerItem *verticalSpacer_2;
    QFrame *frame;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(600, 400);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setMinimumSize(QSize(600, 400));
        MainWindow->setMaximumSize(QSize(600, 400));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        horizontalLayoutWidget = new QWidget(centralwidget);
        horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(10, 10, 571, 331));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        lcdNumber = new QLCDNumber(horizontalLayoutWidget);
        lcdNumber->setObjectName(QString::fromUtf8("lcdNumber"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(lcdNumber->sizePolicy().hasHeightForWidth());
        lcdNumber->setSizePolicy(sizePolicy1);

        verticalLayout_2->addWidget(lcdNumber);

        gridLayout_4 = new QGridLayout();
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        pushButton = new QPushButton(horizontalLayoutWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(pushButton->sizePolicy().hasHeightForWidth());
        pushButton->setSizePolicy(sizePolicy2);
        pushButton->setCheckable(true);
        pushButton->setChecked(false);

        gridLayout_4->addWidget(pushButton, 1, 0, 1, 1);

        ZNUVoltage = new QDial(horizontalLayoutWidget);
        ZNUVoltage->setObjectName(QString::fromUtf8("ZNUVoltage"));
        ZNUVoltage->setMouseTracking(false);
        ZNUVoltage->setFocusPolicy(Qt::WheelFocus);
        ZNUVoltage->setMinimum(500);
        ZNUVoltage->setMaximum(4500);
        ZNUVoltage->setSingleStep(10);
        ZNUVoltage->setPageStep(10);
        ZNUVoltage->setTracking(true);
        ZNUVoltage->setWrapping(false);
        ZNUVoltage->setNotchTarget(10.000000000000000);
        ZNUVoltage->setNotchesVisible(true);

        gridLayout_4->addWidget(ZNUVoltage, 0, 0, 1, 1);


        verticalLayout_2->addLayout(gridLayout_4);


        horizontalLayout->addLayout(verticalLayout_2);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        labelLeftSaved = new QLabel(horizontalLayoutWidget);
        labelLeftSaved->setObjectName(QString::fromUtf8("labelLeftSaved"));

        gridLayout_2->addWidget(labelLeftSaved, 3, 2, 1, 1);

        labelActual3 = new QLabel(horizontalLayoutWidget);
        labelActual3->setObjectName(QString::fromUtf8("labelActual3"));

        gridLayout_2->addWidget(labelActual3, 4, 1, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        gridLayout_2->addItem(verticalSpacer, 5, 0, 1, 1);

        label_12 = new QLabel(horizontalLayoutWidget);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        gridLayout_2->addWidget(label_12, 1, 3, 1, 1);

        pushButton_SaveCentre = new QPushButton(horizontalLayoutWidget);
        pushButton_SaveCentre->setObjectName(QString::fromUtf8("pushButton_SaveCentre"));

        gridLayout_2->addWidget(pushButton_SaveCentre, 4, 3, 1, 1);

        label_11 = new QLabel(horizontalLayoutWidget);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        gridLayout_2->addWidget(label_11, 1, 2, 1, 1);

        pushButton_SaveLeft = new QPushButton(horizontalLayoutWidget);
        pushButton_SaveLeft->setObjectName(QString::fromUtf8("pushButton_SaveLeft"));

        gridLayout_2->addWidget(pushButton_SaveLeft, 3, 3, 1, 1);

        labelActual = new QLabel(horizontalLayoutWidget);
        labelActual->setObjectName(QString::fromUtf8("labelActual"));

        gridLayout_2->addWidget(labelActual, 2, 1, 1, 1);

        labelRightSaved = new QLabel(horizontalLayoutWidget);
        labelRightSaved->setObjectName(QString::fromUtf8("labelRightSaved"));

        gridLayout_2->addWidget(labelRightSaved, 2, 2, 1, 1);

        labelCenterSaved = new QLabel(horizontalLayoutWidget);
        labelCenterSaved->setObjectName(QString::fromUtf8("labelCenterSaved"));

        gridLayout_2->addWidget(labelCenterSaved, 4, 2, 1, 1);

        labelActual2 = new QLabel(horizontalLayoutWidget);
        labelActual2->setObjectName(QString::fromUtf8("labelActual2"));

        gridLayout_2->addWidget(labelActual2, 3, 1, 1, 1);

        label_8 = new QLabel(horizontalLayoutWidget);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        gridLayout_2->addWidget(label_8, 3, 0, 1, 1);

        label_7 = new QLabel(horizontalLayoutWidget);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        gridLayout_2->addWidget(label_7, 2, 0, 1, 1);

        label_10 = new QLabel(horizontalLayoutWidget);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        gridLayout_2->addWidget(label_10, 1, 1, 1, 1);

        pushButton_SaveRight = new QPushButton(horizontalLayoutWidget);
        pushButton_SaveRight->setObjectName(QString::fromUtf8("pushButton_SaveRight"));

        gridLayout_2->addWidget(pushButton_SaveRight, 2, 3, 1, 1);

        label_9 = new QLabel(horizontalLayoutWidget);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        gridLayout_2->addWidget(label_9, 4, 0, 1, 1);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(verticalSpacer_3);

        labelDegrees = new QLabel(horizontalLayoutWidget);
        labelDegrees->setObjectName(QString::fromUtf8("labelDegrees"));
        QFont font;
        font.setPointSize(28);
        labelDegrees->setFont(font);
        labelDegrees->setTextFormat(Qt::RichText);
        labelDegrees->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(labelDegrees);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(verticalSpacer_2);


        gridLayout_2->addLayout(verticalLayout_4, 0, 0, 1, 4);


        horizontalLayout->addLayout(gridLayout_2);

        frame = new QFrame(centralwidget);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setGeometry(QRect(420, 0, 120, 80));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 600, 21));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        pushButton->setText(QCoreApplication::translate("MainWindow", "\320\230\320\275\320\262\320\265\321\200\321\202\320\270\321\200\320\276\320\262\320\260\321\202\321\214", nullptr));
        labelLeftSaved->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        labelActual3->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_12->setText(QString());
        pushButton_SaveCentre->setText(QCoreApplication::translate("MainWindow", "\320\241\320\276\321\205\321\200\320\260\320\275\320\270\321\202\321\214", nullptr));
        label_11->setText(QCoreApplication::translate("MainWindow", "\320\241\320\276\321\205\321\200\320\260\320\275\320\265\320\275\320\276", nullptr));
        pushButton_SaveLeft->setText(QCoreApplication::translate("MainWindow", "\320\241\320\276\321\205\321\200\320\260\320\275\320\270\321\202\321\214", nullptr));
        labelActual->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        labelRightSaved->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        labelCenterSaved->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        labelActual2->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label_8->setText(QCoreApplication::translate("MainWindow", "\320\233\320\265\320\262\321\213\320\271 \320\277\320\276\320\262\320\276\321\200\320\276\321\202, \320\222", nullptr));
        label_7->setText(QCoreApplication::translate("MainWindow", "\320\237\321\200\320\260\320\262\321\213\320\271 \320\277\320\276\320\262\320\276\321\200\320\276\321\202, \320\222", nullptr));
        label_10->setText(QCoreApplication::translate("MainWindow", "\320\242\320\265\320\272\321\203\321\211\320\265\320\265", nullptr));
        pushButton_SaveRight->setText(QCoreApplication::translate("MainWindow", "\320\241\320\276\321\205\321\200\320\260\320\275\320\270\321\202\321\214", nullptr));
        label_9->setText(QCoreApplication::translate("MainWindow", "\320\246\320\265\320\275\321\202\321\200, \320\222", nullptr));
        labelDegrees->setText(QCoreApplication::translate("MainWindow", "\320\243\320\263\320\276\320\273", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
