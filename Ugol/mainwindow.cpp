#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->lcdNumber->setDigitCount(4);
    ui->ZNUVoltage->setPageStep(100);
    ui->labelActual->setText(QString::number((long)ui->ZNUVoltage->value()/1000.0f,'f', 2));
    ui->labelActual2->setText(QString::number(ui->ZNUVoltage->value()/1000.0f,'f', 2));
    ui->labelActual3->setText(QString::number(ui->ZNUVoltage->value()/1000.0f, 'f', 2));

    if (ui->ZNUVoltage->value() == centerSaved)
    {
        ui->labelDegrees->setText("0°");
    }
    else if (ui->ZNUVoltage->value() > centerSaved)
    {
        ui->labelDegrees->setText(QString::number(this->calculateDegrees(rightSaved, centerSaved, ui->ZNUVoltage->value()), 'f', 0) + "°");
    }
    else if (ui->ZNUVoltage->value() < centerSaved)
    {
        ui->labelDegrees->setText(QString::number(this->calculateDegrees(leftSaved, centerSaved, ui->ZNUVoltage->value()), 'f', 0) + "°");
    }

    ui->labelRightSaved->setText(QString::number(rightSaved/1000.0f, 'f', 2));
    ui->labelLeftSaved->setText(QString::number(leftSaved/1000.0f, 'f', 2));
    ui->labelCenterSaved->setText(QString::number(centerSaved/1000.0f, 'f', 2));



}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_ZNUVoltage_valueChanged(int value)
{
    float VoltageValue = value/1000.0f;
    ui->lcdNumber->display(VoltageValue);
    ui->labelActual->setText(QString::number(VoltageValue,'f', 2));
    ui->labelActual2->setText(QString::number(VoltageValue,'f', 2));
    ui->labelActual3->setText(QString::number(VoltageValue, 'f', 2));
    if (value == centerSaved)
    {
        ui->labelDegrees->setText("0°");
    }
    else if (value > centerSaved)
    {
        ui->labelDegrees->setText(QString::number(this->calculateDegrees(rightSaved, centerSaved, value), 'f', 0) + "°");
    }
    else if (value < centerSaved)
    {
        ui->labelDegrees->setText(QString::number(this->calculateDegrees(leftSaved, centerSaved, value), 'f', 0) + "°");
    }
}

void MainWindow::on_pushButton_SaveRight_clicked()
{
    MainWindow::rightSaved = ui->ZNUVoltage->value();
    ui->labelRightSaved->setText(QString::number(rightSaved/1000.0f,'f', 2));
}

void MainWindow::on_pushButton_SaveLeft_clicked()
{
    MainWindow::leftSaved = ui->ZNUVoltage->value();
    ui->labelLeftSaved->setText(QString::number(leftSaved/1000.0f,'f', 2));
}

void MainWindow::on_pushButton_SaveCentre_clicked()
{
    MainWindow::centerSaved = ui->ZNUVoltage->value();
    ui->labelCenterSaved->setText(QString::number(centerSaved/1000.0f,'f', 2));
}

void MainWindow::on_pushButton_clicked()
{

}

void MainWindow::on_pushButton_pressed()
{

}

void MainWindow::on_pushButton_released()
{

}


void MainWindow::on_pushButton_clicked(bool checked)
{
    if (checked)
    {
        ui->ZNUVoltage->setValue(ui->ZNUVoltage->maximum() - ui->ZNUVoltage->value());
        ui->ZNUVoltage->setInvertedAppearance(true);
    }
    else
    {
        ui->ZNUVoltage->setValue(ui->ZNUVoltage->maximum() - ui->ZNUVoltage->value());
        ui->ZNUVoltage->setInvertedAppearance(false);
    }
}

float MainWindow::calculateDegrees(int Umax, int Ucenter, int Udata)
{
    return (std::abs(Udata - Ucenter) * (MAX_DEGREE/(float)(std::abs(Umax - Ucenter))));
}
