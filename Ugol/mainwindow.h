#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#define MAX_DEGREE 38

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_ZNUVoltage_valueChanged(int value);

    void on_pushButton_SaveRight_clicked();

    void on_pushButton_SaveLeft_clicked();

    void on_pushButton_SaveCentre_clicked();

    void on_pushButton_clicked();

    void on_pushButton_pressed();

    void on_pushButton_released();

    void on_pushButton_clicked(bool checked);

private:
    Ui::MainWindow *ui;
    bool buttonInverseState = 0;
    int rightSaved = 4500;
    int leftSaved = 500;
    int centerSaved = 2500;
    float calculateDegrees(int Umax, int Ucenter, int Udata);
};
#endif // MAINWINDOW_H
